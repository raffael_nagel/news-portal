<?php

  if(session_id() == '' || !isset($_SESSION)) {
      ob_start();
      ini_set('session.gc_maxlifetime', '28800');
      session_start();
  }
  if (!isset($_SESSION['LOGGED_IN']) || !$_SESSION['LOGGED_IN']) {
    header("Location: login.html");
  }

  require_once(dirname(__FILE__) ."/sys/api.php");

  // call to get user info
  $user = PortalAPI::getUserInfo($_SESSION['USER_TOKEN']);
  $user_category_ids = [];
  foreach ($user->categories as $category) {
    $user_category_ids[] = $category->id;
  }

  $categories = PortalAPI::getCategories($_SESSION['USER_TOKEN']);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured portal of news">
        <meta name="author" content="Raffael Nagel">

        <!-- <link rel="shortcut icon" href="img/favicon_1.ico"> -->

        <title>Portal</title>

        <!-- Google-Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic' rel='stylesheet'>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-reset.css" rel="stylesheet">

        <!--Animation css-->
        <link href="css/animate.css" rel="stylesheet">

        <!--Icon-fonts css-->
        <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="assets/ionicon/css/ionicons.min.css" rel="stylesheet" />

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="assets/morris/morris.css">

        <!--Form Wizard-->
        <link rel="stylesheet" type="text/css" href="assets/form-wizard/jquery.steps.css" />


        <!-- sweet alerts -->
        <link href="assets/sweet-alert/sweet-alert.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/style.css" rel="stylesheet">
        <link href="css/helper.css" rel="stylesheet">
        <link href="css/style-responsive.css" rel="stylesheet" />

        <link href="assets/notifications/notification.css" rel="stylesheet" />
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>


    <body>

        <!-- Aside Start-->
        <aside class="left-panel">

            <!-- brand -->
            <div class="logo">
                <a href="index.php" class="logo-expanded">
                    <img src="img/single-logo.png" alt="logo">
                    <span class="nav-label">Portal</span>
                </a>
            </div>
            <!-- / brand -->

            <!-- Navbar Start -->
            <nav class="navigation">
                <ul class="list-unstyled">
                    <li class="has-submenu"><a href="index.php"><i class="ion-home"></i> <span class="nav-label">Dashboard</span></a>
                    </li>
                    <li class="has-submenu active"><a href="#"><i class="ion-person"></i> <span class="nav-label">Profile</span></a>
                    </li>
                </ul>
            </nav>

        </aside>
        <!-- Aside Ends-->


        <!--Main Content Start -->
        <section class="content">

            <!-- Header -->
            <header class="top-head container-fluid">
                <button type="button" class="navbar-toggle pull-left">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Right navbar -->
                <ul class="list-inline navbar-right top-menu top-right-menu">
                    <!-- user login dropdown start-->
                    <li class="dropdown text-center">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="username"><?php echo $user->firstName;?> </span> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu extended pro-menu fadeInUp animated" tabindex="5003" style="overflow: hidden; outline: none;">
                            <li><a href="#" onclick="logout()"><i class="fa fa-sign-out"></i> Log Out</a></li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- End right navbar -->

            </header>
            <!-- Header Ends -->


            <!-- Page Content Start -->
            <!-- ================== -->

            <div class="wraper container-fluid">
                <!-- <div class="page-title">
                    <h3 class="title">Profile</h3>
                </div> -->

                <!-- Wizard with Validation -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Profile</h3>
                            </div>
                            <div class="panel-body">
                                <form id="wizard-validation-form" action="#">
                                    <div>
                                        <h3>User Info</h3>
                                        <section>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="lastname">Email *</label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" id="email" name="email" type="email" value="<?php echo $user->email;?>">
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="firstname">First name *</label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" id="firstname" name="firstname" type="text"  value="<?php echo $user->firstName;?>">
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="lastname">Last name </label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" id="lastname" name="lastname" type="text" value="<?php echo $user->lastName;?>">
                                                </div>
                                            </div>

                                            <div class="form-group clearfix">
                                                <label class="col-lg-12 control-label ">(*) Mandatory</label>
                                            </div>
                                        </section>
                                        <h3>Preferences</h3>
                                        <section>
                                            <div class="form-group clearfix">
                                              <?php
                                                foreach ($categories as $category) {
                                                  $checked = in_array($category->id, $user_category_ids);
                                                  echo "<div class=\"col-lg-4\">";
                                                  echo "<div class=\"checkbox\">";
                                                  echo "<label class=\"cr-styled\">";
                                                  if ($checked) {
                                                    echo "<input type=\"checkbox\" checked=\"\" id=\"".$category->id."\" name=\"category-".$category->name."\">";
                                                  } else {
                                                    echo "<input type=\"checkbox\" id=\"".$category->id."\" name=\"category-".$category->name."\">";
                                                  }
                                                  echo "<i class=\"fa\"></i>".ucwords($category->name);
                                                  echo "</label></div></div>";
                                                }
                                              ?>
                                              <input class="form-control hide" id="categories" name="categories" type="text" value="">
                                              <input class="form-control hide" id="user" name="user" type="text" value="<?php echo $user->id;?>">
                                            </div>

                                        </section>
                                    </div>
                                </form>
                            </div>  <!-- End panel-body -->
                        </div> <!-- End panel -->

                    </div> <!-- end col -->

                </div> <!-- End row -->

            <!-- Page Content Ends -->

            </div>
            <!-- Page Content Ends -->
            <!-- ================== -->

            <!-- Footer Start -->
            <footer class="footer">
                2018 © ITU.
            </footer>
            <!-- Footer Ends -->



        </section>
        <!-- Main Content Ends -->



        <!-- js placed at the end of the document so the pages load faster -->
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/pace.min.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.scrollTo.min.js"></script>
        <script src="js/jquery.nicescroll.js"></script>
        <script src="assets/chat/moment-2.2.1.js"></script>

        <!-- Counter-up -->
        <script src="js/waypoints.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>

        <!-- EASY PIE CHART JS -->
        <script src="assets/easypie-chart/easypiechart.min.js"></script>
        <script src="assets/easypie-chart/jquery.easypiechart.min.js"></script>
        <script src="assets/easypie-chart/example.js"></script>


        <!--C3 Chart-->
        <script src="assets/c3-chart/d3.v3.min.js"></script>
        <script src="assets/c3-chart/c3.js"></script>

        <!--Morris Chart-->
        <script src="assets/morris/morris.min.js"></script>
        <script src="assets/morris/raphael.min.js"></script>

        <!-- sparkline -->
        <script src="assets/sparkline-chart/jquery.sparkline.min.js"></script>
        <script src="assets/sparkline-chart/chart-sparkline.js"></script>

        <!-- sweet alerts -->
        <script src="assets/sweet-alert/sweet-alert.min.js"></script>
        <script src="assets/sweet-alert/sweet-alert.init.js"></script>

        <script src="js/jquery.app.js"></script>
        <!-- Chat -->
        <script src="js/jquery.chat.js"></script>
        <!-- Dashboard -->
        <script src="js/jquery.dashboard.js"></script>

        <!--Form Validation-->
        <script src="assets/form-wizard/bootstrap-validator.min.js" type="text/javascript"></script>

        <!--Form Wizard-->
        <script src="assets/form-wizard/jquery.steps.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/jquery.validate/jquery.validate.min.js"></script>

        <!--wizard initialization-->
        <script src="assets/form-wizard/wizard-init.js" type="text/javascript"></script>

        <script src="js/portal-functions.js"></script>

        <script src="assets/notifications/notify.min.js"></script>
        <script src="assets/notifications/notify-metro.js"></script>
        <script src="assets/notifications/notifications.js"></script>

        <script>
        /* ==============================================
             Counter Up
             =============================================== */
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                    delay: 100,
                    time: 1200
                });
            });
            
        </script>


    </body>
</html>
