<?php

if(session_id() == '' || !isset($_SESSION)) {
    ob_start();
    ini_set('session.gc_maxlifetime', '28800');
    session_start();
}

require_once(dirname(__FILE__) ."/sys/api.php");

$security_questions = PortalAPI::getSecurityQuestions();

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured portal of news">
        <meta name="author" content="Raffael Nagel">

        <!-- <link rel="shortcut icon" href="img/favicon_1.ico"> -->

        <title>Portal</title>

        <!-- Google-Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic' rel='stylesheet'>


        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-reset.css" rel="stylesheet">

        <!--Animation css-->
        <link href="css/animate.css" rel="stylesheet">

        <!--Icon-fonts css-->
        <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="assets/ionicon/css/ionicons.min.css" rel="stylesheet" />

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="assets/morris/morris.css">


        <!-- Custom styles for this template -->
        <link href="css/style.css" rel="stylesheet">
        <link href="css/helper.css" rel="stylesheet">
        <link href="css/style-responsive.css" rel="stylesheet" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>


    <body>

        <div class="wrapper-page animated fadeInDown">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading">
                   <h3 class="text-center m-t-10"> Create a new Account </h3>
                </div>

                <form class="form-horizontal m-t-40" action="" id="register-form">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" id="new-email" name="new-email" type="email" required="" placeholder="*Email">
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control " id="firstname" name="firstname" type="text" required="" placeholder="*First Name">
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control " id="lastname" name="lastname" type="text" required="" placeholder="Last Name">
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <select class="form-control " id="security-question-1" name="security-question-1" required="">
                              <option value="invalid">Security Question 1</option>
                              <?php
                                foreach ($security_questions as $question) {
                                  echo '<option value="'.$question->id.'">'.ucfirst($question->question).'</option>';
                                }
                              ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control " id="security-answer-1" name="security-answer-1" type="text" required="" placeholder="*Answer">
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <select class="form-control " id="security-question-2" name="security-question-2" required="">
                              <option value="invalid">Security Question 2</option>
                              <?php
                                foreach ($security_questions as $question) {
                                  echo '<option value="'.$question->id.'">'.ucfirst($question->question).'</option>';
                                }
                              ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control " id="security-answer-2" name="security-answer-2" type="text" required="" placeholder="*Answer">
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <select class="form-control " id="security-question-3" name="security-question-3" required="">
                              <option value="invalid">Security Question 3</option>
                              <?php
                                foreach ($security_questions as $question) {
                                  echo '<option value="'.$question->id.'">'.ucfirst($question->question).'</option>';
                                }
                              ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control " id="security-answer-3" name="security-answer-3" type="text" required="" placeholder="*Answer">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control " id="new-password" name="new-password" type="password" required="" placeholder="*Password">
                        </div>
                    </div>


                    <div class="alert alert-danger hide" id="register-invalid-msg" name="register-invalid-msg">
                      Fields with (*) are required.
                    </div>

                    <div class="form-group text-right">
                        <div class="col-xs-12">
                            <button class="btn btn-purple w-md" type="button" onclick="register()">Register</button>
                        </div>
                    </div>

                    <div class="form-group m-t-30">
                        <div class="col-sm-12 text-center">
                            <a href="login.html">Already have account?</a>
                        </div>
                    </div>
                </form>

            </div>
        </div>

        <!-- js placed at the end of the document so the pages load faster -->
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/pace.min.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.nicescroll.js" type="text/javascript"></script>

        <!--common script for all pages-->
        <script src="js/jquery.app.js"></script>

        <script src="js/portal-functions.js"></script>


    </body>
</html>
