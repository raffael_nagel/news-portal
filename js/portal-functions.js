// News portal js functions

function login(){
  const username = document.getElementById('username').value;
  const password = document.getElementById('password').value;

  if (username == undefined || password == '') {
    $('#invalid-msg').removeClass("hide");
    return;
  }
  $('#invalid-msg').addClass("hide");
  $.ajax({
    type: 'POST',
    url: 'sys/login.php',
    data: $('#login-form').serialize(),
    success: function (result) {
      if (result) {
        console.log(result);
        window.location.href = 'index.php';
        $('#login-form')[0].reset();
        $('#invalid-msg').addClass("hide");
      } else {
          $('#invalid-msg').removeClass("hide");
      }
    },
    error: function (error) {
      console.log(error);
    }
  });
}

function logout(){
  $.ajax({
    type: 'POST',
    url: 'sys/logout.php',
    data: '',
    success: function (result) {
      window.location.href = 'index.php';
    },
    error: function (error) {
      console.log(error);
    }
  });
}

function register(){
  const email = document.getElementById('new-email').value;
  const firstname = document.getElementById('firstname').value;
  const lastname = document.getElementById('lastname').value;
  const security_question_1 = document.getElementById('security-question-1').value;
  const security_answer_1 = document.getElementById('security-answer-1').value;
  const security_question_2 = document.getElementById('security-question-2').value;
  const security_answer_2 = document.getElementById('security-answer-2').value;
  const security_question_3 = document.getElementById('security-question-3').value;
  const security_answer_3 = document.getElementById('security-answer-3').value;
  const password = document.getElementById('new-password').value;

  if (
    firstname == undefined ||
    password == '' ||
    email == '' ||
    security_question_1 == 'invalid' ||
    security_question_2 == 'invalid' ||
    security_question_3 == 'invalid'
  ) {
    $('#register-invalid-msg').removeClass("hide");
    return;
  }

  $('#register-invalid-msg').addClass("hide");
  $.ajax({
    type: 'POST',
    url: 'sys/create-user.php',
    data: $('#register-form').serialize(),
    success: function (result) {
      console.log(result);
      window.location.href = 'index.php';
      $('#register-form')[0].reset();
    },
    error: function (error) {
      console.log(error);
    }
  });
}

function recoverpw(){
  const email = document.getElementById('pwrec-email').value;
  const question = document.getElementById('security-question-id').value;
  const answer = document.getElementById('security-answer').value;
  const password = document.getElementById('password').value;

  if (question == undefined || password == '' || answer == '') {
    $('#recover-invalid-msg').removeClass("hide");
    return;
  }
  $('#recover-invalid-msg').addClass("hide");
  $('#recover-valid-msg').addClass("hide");
  $.ajax({
    type: 'POST',
    url: 'sys/recoverpw.php',
    data: $('#recover-form').serialize(),
    success: function (result) {
      console.log(result);
      if (result) {
        $('#recover-invalid-msg').addClass("hide");
        $('#recover-valid-msg').removeClass("hide");
      } else {
        $('#recover-valid-msg').addClass("hide");
        $('#recover-invalid-msg').removeClass("hide");
      }
    },
    error: function (error) {
      console.log(error);
      $('#recover-invalid-msg').removeClass("hide");
      $('#recover-valid-msg').addClass("hide");
    }
  });
}

function showArticle(id) {
  $.ajax({
    type: 'POST',
    url: 'sys/load-article.php',
    data: {'id': id},
    success: function (result) {
      console.log(result);
      result = JSON.parse(result);
      const article = result.article;
      $('#article-title').text(article.headLine);
      $('#article-date').text(article.date + ', ' + article.city);
      $('#article-desc').text(article.text);
      $('#article-link').text(article.sourceLink);
    },
    error: function (error) {
      $("body").trigger("click");
      console.log(error);
    }
  });
}

function goToArticle() {
  window.open($('#article-link').text());
  return false;
}

function getQuestion() {
  const email = document.getElementById('pwrec-email').value;
  $.ajax({
    type: 'POST',
    url: 'sys/recover-security-question.php',
    data: {'email': email},
    success: function (result) {
      $('#recover-invalid-email-msg').addClass('hide');
      result = JSON.parse(result);
      console.log(result);
      const question = result.question.question;
      console.log(question);
      $('#security-question').text(question.question);
      $('#security-question-id').val(question.id);
      $('#user-id').val(result.question.userId);
      $('#security-question').removeClass('hide');
      $('#security-answer').removeClass('hide');
      $('#password').removeClass('hide');
      $('#password').removeClass('hide');
      $('#recover-pass-next').addClass('hide');
      $('#recover-pass-btn').removeClass('hide');
    },
    error: function (error) {
      $('#recover-invalid-email-msg').removeClass('hide');
      console.log(error);
    }
  });
}

function updateProfile(){
  const email = document.getElementById('email').value;
  const firstname = document.getElementById('firstname').value;
  const lastname = document.getElementById('lastname').value;

  var categories = [];
  $("input:checked").map(
    function() {
      categories.push(this.id);
    }
  );
  $("#categories").val(categories.join(','));

  $.ajax({
    type: 'POST',
    url: 'sys/update-preferences.php',
    data: $('#wizard-validation-form').serialize(),
    success: function (result) {
      console.log(result);
      window.location.href = 'index.php';
      $.Notification.notify('success', 'top right', 'Profile Updated', 'Your profile was successfully updated.');
    },
    error: function (error) {
      console.log(error);
    }
  });
}
