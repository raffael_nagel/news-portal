<?php

  require_once(dirname(__FILE__) ."/api.php");

  $email = isset($_POST['new-email']) ? $_POST['new-email'] : null;
  $firstname = isset($_POST['firstname']) ? $_POST['firstname'] : null;
  $lastname = isset($_POST['lastname']) ? $_POST['lastname'] : null;
  $question1 = isset($_POST['security-question-1']) ? $_POST['security-question-1'] : null;
  $question2 = isset($_POST['security-question-2']) ? $_POST['security-question-2'] : null;
  $question3 = isset($_POST['security-question-3']) ? $_POST['security-question-3'] : null;
  $answer1 = isset($_POST['security-answer-1']) ? $_POST['security-answer-1'] : null;
  $answer2 = isset($_POST['security-answer-2']) ? $_POST['security-answer-2'] : null;
  $answer3 = isset($_POST['security-answer-3']) ? $_POST['security-answer-3'] : null;
  $password = isset($_POST['new-password']) ? $_POST['new-password'] : null;

  if (!$firstname || !$password || !$email) {
    echo json_encode(['result' => 'error']);
    exit;
  }

  $data = [
    'Email' => $email,
    'Password' => $password,
    'FirstName' => $firstname,
    'LastName' => $lastname,
    'SecurityAnswers' => [
      ['QuestionId' => $question1, 'Answer' => $answer1],
      ['QuestionId' => $question2, 'Answer' => $answer2],
      ['QuestionId' => $question3, 'Answer' => $answer3],
    ],
  ];

  // Call to create user
  $response = PortalAPI::register($data);

  if ($response) {
    $_SESSION['LOGGED_IN'] = true;
    $_SESSION['USER_TOKEN'] = $response->token;
    $_SESSION['USER_TOKEN_EXPIRATION'] = $response->expireAt;
    echo json_encode([
      'result' => $response,
    ]);
  } else {
    echo json_encode(['result' => 'failure', 'response' => $response, 'data' => $data]);
  }
?>
