<?php

  require_once(dirname(__FILE__) ."/api.php");
  if(session_id() == '' || !isset($_SESSION)) {
      ob_start();
      ini_set('session.gc_maxlifetime', '28800');
      session_start();
  }

  $email = isset($_POST['email']) ? $_POST['email'] : null;
  $firstname = isset($_POST['firstname']) ? $_POST['firstname'] : null;
  $lastname = isset($_POST['lastname']) ? $_POST['lastname'] : null;
  $categories = isset($_POST['categories']) ? $_POST['categories'] : null;
  $user = isset($_POST['user']) ? $_POST['user'] : null;

  if (!$firstname || !$email) {
    echo json_encode(['result' => 'error']);
    exit;
  }

  //Call to update preferences
  $data = [
    'email' => $email,
    'firstName' => $firstname,
    'lastName' => $lastname,
    'id' => $user,
  ];
  $result1 = PortalAPI::updateUserInfo($_SESSION['USER_TOKEN'], $data);
  $result2 = PortalAPI::updateUserPreferences($_SESSION['USER_TOKEN'], ['categoryIds' => explode(',', $categories)]);

  if (true) {
    echo json_encode([
      'result' => $result1,
    ]);
  } else {
    echo json_encode(['result' => 'failure']);
  }
?>
