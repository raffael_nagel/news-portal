<?php
  require_once(dirname(__FILE__) ."/api.php");
  $email = isset($_POST['email']) ? $_POST['email'] : null;

  if (!$email) {
    echo json_encode(['result' => 'error']);
    exit;
  }

  // Call to get Answer
  $response = PortalAPI::getSecurityQuestion($email);

  if ($response) {
    echo json_encode([
      'result' => 'success',
      'question' => $response[0],
    ]);
  } else {
    echo json_encode(['result' => 'failure']);
  }
?>
