<?php
  if(session_id() == '' || !isset($_SESSION)) {
      ob_start();
      ini_set('session.gc_maxlifetime', '28800');
      session_start();
  }

  $_SESSION['LOGGED_IN'] = false;
  $_SESSION['USER_TOKEN'] = '';

  echo json_encode([
    'result' => 'success',
  ]);
?>
