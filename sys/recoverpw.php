<?php
  require_once(dirname(__FILE__) ."/api.php");

  $email = isset($_POST['pwrec-email']) ? $_POST['pwrec-email'] : null;
  $user = isset($_POST['user-id']) ? $_POST['user-id'] : null;
  $question = isset($_POST['security-question-id']) ? $_POST['security-question-id'] : null;
  $answer = isset($_POST['security-answer']) ? $_POST['security-answer'] : null;
  $password = isset($_POST['password']) ? $_POST['password'] : null;

  // Call to update password
  $data = [
    'newPassword' => $password,
    'securityAnswers' => [['userId' => $user, 'questionId' => $question, 'answer' => $answer]],
  ];

  $response = PortalAPI::updatePassword($data);

  echo $response;
?>
