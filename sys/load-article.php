<?php

  require_once(dirname(__FILE__) ."/api.php");

  if(session_id() == '' || !isset($_SESSION)) {
      ob_start();
      ini_set('session.gc_maxlifetime', '28800');
      session_start();
  }
  $id = isset($_POST['id']) ? $_POST['id'] : null;

  if (!$id) {
    echo json_encode(['result' => 'error']);
    exit;
  }

  // Call to get article info
  $article = PortalAPI::getArticlesById($_SESSION['USER_TOKEN'], $id);

  if ($article) {
    $article->date = date('l jS \of F Y',strtotime($article->date));
    echo json_encode([
      'result' => 'success',
      'article' => $article,
    ]);
  } else {
    echo json_encode(['result' => 'failure']);
  }
?>
