<?php
  require_once(dirname(__FILE__) ."/api.php");

  if(session_id() == '' || !isset($_SESSION)) {
      ob_start();
      ini_set('session.gc_maxlifetime', '28800');
      session_start();
  }
  $user = isset($_POST['username']) ? $_POST['username'] : null;
  $password = isset($_POST['password']) ? $_POST['password'] : null;

  if (!$user || !$password) {
    echo json_encode(['result' => 'error']);
    exit;
  }

  // Call to get login token
  $response = PortalAPI::loginCall($user, $password);

  if ($response->token) {
    $_SESSION['LOGGED_IN'] = true;
    $_SESSION['USER_TOKEN'] = $response->token;
    $_SESSION['USER_TOKEN_EXPIRATION'] = $response->expireAt;
    echo true;
  } else {
    $_SESSION['LOGGED_IN'] = false;
    $_SESSION['USER_TOKEN'] = null;
    $_SESSION['USER_TOKEN_EXPIRATION'] = null;
    echo false;
  }
?>
