<?php

  if(session_id() == '' || !isset($_SESSION)) {
      ob_start();
      ini_set('session.gc_maxlifetime', '28800');
      session_start();
  }
  if (!isset($_SESSION['LOGGED_IN']) || !$_SESSION['LOGGED_IN']) {
    header("Location: login.html");
  }

  require_once(dirname(__FILE__) ."/sys/api.php");

  // call to get user info
  $user = PortalAPI::getUserInfo($_SESSION['USER_TOKEN']);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured portal of news">
        <meta name="author" content="Raffael Nagel">

        <!-- <link rel="shortcut icon" href="img/favicon_1.ico"> -->

        <title>Portal</title>

        <!-- Google-Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic' rel='stylesheet'>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-reset.css" rel="stylesheet">

        <!--Animation css-->
        <link href="css/animate.css" rel="stylesheet">

        <!-- Plugins css -->
        <link href="assets/modal-effect/css/component.css" rel="stylesheet">


        <!--Icon-fonts css-->
        <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="assets/ionicon/css/ionicons.min.css" rel="stylesheet" />

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="assets/morris/morris.css">

        <!-- sweet alerts -->
        <link href="assets/sweet-alert/sweet-alert.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/style.css" rel="stylesheet">
        <link href="css/helper.css" rel="stylesheet">
        <link href="css/style-responsive.css" rel="stylesheet" />


        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>


    <body>

        <!-- Aside Start-->
        <aside class="left-panel">

            <!-- brand -->
            <div class="logo">
                <a href="index.php" class="logo-expanded">
                    <img src="img/single-logo.png" alt="logo">
                    <span class="nav-label">Portal</span>
                </a>
            </div>
            <!-- / brand -->

            <!-- Navbar Start -->
            <nav class="navigation">
                <ul class="list-unstyled">
                    <li class="has-submenu active"><a href="#"><i class="ion-home"></i> <span class="nav-label">Dashboard</span></a>
                    </li>
                    <li class="has-submenu"><a href="profile.php"><i class="ion-person"></i> <span class="nav-label">Profile</span></a>
                    </li>
                </ul>
            </nav>

        </aside>
        <!-- Aside Ends-->


        <!--Main Content Start -->
        <section class="content">

            <!-- Header -->
            <header class="top-head container-fluid">
                <button type="button" class="navbar-toggle pull-left">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Right navbar -->
                <ul class="list-inline navbar-right top-menu top-right-menu">
                    <!-- user login dropdown start-->
                    <li class="dropdown text-center">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="username"><?php echo $user->firstName;?> </span> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu extended pro-menu fadeInUp animated" tabindex="5003" style="overflow: hidden; outline: none;">
                            <li><a href="#" onclick="logout()"><i class="fa fa-sign-out"></i> Log Out</a></li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- End right navbar -->

            </header>
            <!-- Header Ends -->


            <!-- Page Content Start -->
            <!-- ================== -->

            <div class="wraper container-fluid">
                <div class="page-title">
                    <h3 class="title">Hello <?php echo $user->firstName;?>, here are the news for today!</h3>
                </div>

                <?php
                  $colors = ['bg-primary', 'bg-success', 'bg-danger', 'bg-inverse', 'bg-purple', 'bg-pink'];
                  if (count($user->categories) < 1) {
                    echo '<div class="row">';
                    echo '<div class="col-lg-12 col-sm-12">';
                    echo '<div class="widget-panel widget-style-2 bg-primary">';
                    echo '<div>No categories to show. Please update your profile.</div>';
                    echo '</div></div></div>';

                  }
                  foreach ($user->categories as $category) {
                    $articles = PortalAPI::getArticlesByCategory($_SESSION['USER_TOKEN'], $category->id);
                    if (count($articles) < 1) continue;

                    echo '<div class="row">';
                    echo '<div class="col-lg-12 col-sm-12">';
                    echo '<div class="widget-panel widget-style-2 '.$colors[rand(0,5)].'">';
                    echo '<i class="ion-ios7-paper-outline"></i>';
                    echo '<h3>'.ucwords($category->name).'</h3>';
                    echo '<div></div>';
                    echo '</div></div></div>';

                    // news
                    echo '<div class="row">';
                    $n = 0;
                    $limit = 9;
                    foreach ($articles as $article) {
                      if ($n >= 9) break;
                      $n++;
                      echo '<div class="col-md-4">';
                      echo '<div class="panel panel-default text-center" style="min-height: 295px;">';
                      echo '<div class="panel-body p-0">';
                      echo '<div>';
                      $title = $article->headLine;
                      if (strlen($title) > 60) {
                        $title = substr($title, 0, 55);
                        $title .= '..';
                      } else if (strlen($title) < 60) {
                        for ($i = 0; $i < 60 - strlen($title); $i++) {
                          $title .= ' ';
                        }
                      }
                      echo '<h4><a href="#">'.$title.'</a></h4>';
                      $date = date('l jS \of F Y',strtotime($article->date));
                      echo '<p class="small">'.$date.', '.$article->city.'</p>';
                      echo '<p class="m-t-30"><em>'.substr($article->text, 0, 150).'...</em></p>';
                      echo '<a href="javascript:;" onclick="showArticle('.$article->id.')" class="md-trigger btn btn-primary btn-sm m-t-40" data-modal="modal-1">Show Me</a>';
                      echo '</div></div></div></div>';
                    }
                    echo '</div>';
                  }
                ?>


            <!-- Page Content Ends -->

            <div class="md-modal md-effect-1" id="modal-1">
                <div class="md-content">
                    <h3 style="padding-top:40px;font-size:25px" id="article-title"></h3>
                    <div>
                      <h4 id="article-date"></h4>
                      <p class="m-t-10" id="article-desc"></p>
                      <p class="hidden" id="article-link"></p>
                      <button id="article-button" class="md-close btn-sm btn-primary m-t-20" onclick="goToArticle()">Go to Source</button>
                    </div>
                </div>
            </div>
              <div class="md-overlay"></div><!-- the overlay element -->

            </div>
            <!-- Page Content Ends -->
            <!-- ================== -->

            <!-- Footer Start -->
            <footer class="footer">
                2018 © ITU.
            </footer>
            <!-- Footer Ends -->



        </section>
        <!-- Main Content Ends -->



        <!-- js placed at the end of the document so the pages load faster -->
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/pace.min.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.scrollTo.min.js"></script>
        <script src="js/jquery.nicescroll.js"></script>
        <script src="assets/chat/moment-2.2.1.js"></script>

        <!-- Counter-up -->
        <script src="js/waypoints.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>

        <!-- EASY PIE CHART JS -->
        <script src="assets/easypie-chart/easypiechart.min.js"></script>
        <script src="assets/easypie-chart/jquery.easypiechart.min.js"></script>
        <script src="assets/easypie-chart/example.js"></script>


        <!--C3 Chart-->
        <script src="assets/c3-chart/d3.v3.min.js"></script>
        <script src="assets/c3-chart/c3.js"></script>

        <!--Morris Chart-->
        <script src="assets/morris/morris.min.js"></script>
        <script src="assets/morris/raphael.min.js"></script>

        <!-- sparkline -->
        <script src="assets/sparkline-chart/jquery.sparkline.min.js"></script>
        <script src="assets/sparkline-chart/chart-sparkline.js"></script>

        <!-- sweet alerts -->
        <script src="assets/sweet-alert/sweet-alert.min.js"></script>
        <script src="assets/sweet-alert/sweet-alert.init.js"></script>

        <script src="js/jquery.app.js"></script>
        <!-- Chat -->
        <script src="js/jquery.chat.js"></script>
        <!-- Dashboard -->
        <script src="js/jquery.dashboard.js"></script>

        <!-- Modal-Effect -->
        <script src="assets/modal-effect/js/classie.js"></script>
        <script src="assets/modal-effect/js/modalEffects.js"></script>

        <!-- Todo -->
        <script src="js/jquery.todo.js"></script>

        <script src="js/portal-functions.js"></script>


        <script>
        /* ==============================================
             Counter Up
             =============================================== */
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                    delay: 100,
                    time: 1200
                });
            });
        </script>


    </body>
</html>
